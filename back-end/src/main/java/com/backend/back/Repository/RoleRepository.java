package com.backend.back.Repository;

import java.util.Optional;

import com.backend.back.Enum.ERole;
import com.backend.back.Model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
