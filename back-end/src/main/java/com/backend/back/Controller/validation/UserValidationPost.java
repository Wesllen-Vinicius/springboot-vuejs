package com.backend.back.Controller.validation;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserValidationPost {
  @NotNull @NotEmpty
  private String email;
  
  @NotNull @NotEmpty @Length(min = 4)
  private String username;
  
  @NotNull @NotEmpty @Length(min = 8)
  private String password;
  
  public String getEmail() {
    return email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  
}
