package com.backend.back.Controller;

import com.backend.back.Controller.dto.UserDto;
import com.backend.back.Model.User;
import com.backend.back.Repository.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {
  private final UserRepository userRepository;

  public UserController(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @PostMapping("/newUser")
  User newUser(@RequestBody User newUser) {
    return userRepository.save(newUser);
  }

  @GetMapping
  public List<UserDto> list() {
    List<User> users = userRepository.findAll();
    return UserDto.converter(users);
  }

  @GetMapping("/{id}")
  public ResponseEntity<UserDto> detalhar(@PathVariable Long id) {
    Optional<User> user = userRepository.findById(id);
    return user.map(value -> ResponseEntity.ok(new UserDto(value))).orElseGet(() -> ResponseEntity.notFound().build());
  }
  
  @PutMapping("/updateUser/{id}")
  User replaceUser(@RequestBody User newUser, @PathVariable Long id) {
    return userRepository.findById(id)
      .map(user -> {
        user.setEmail(newUser.getEmail());
        user.setUsername(newUser.getUsername());
        user.setPassword(newUser.getPassword());
        user.setRoles(newUser.getRoles());
        return userRepository.save(newUser);
      })
      .orElseGet(() -> {
        newUser.setId(id);
        return userRepository.save(newUser);
      });
  }
  
  @DeleteMapping("/deleteUser/{id}")
  void deleteUser(@PathVariable Long id) {
    userRepository.deleteById(id);
  }
  
}
