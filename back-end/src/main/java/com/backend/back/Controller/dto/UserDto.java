package com.backend.back.Controller.dto;

import com.backend.back.Model.User;
import java.util.List;
import java.util.stream.Collectors;

public class UserDto {
    private Long id;
    private String email;
    private String username;

    public UserDto(User user) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.username = user.getUsername();
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return  email;
    }

    public String getUsername(){
        return username;
    }

    public static List<UserDto> converter(List<User> users) {
        return users.stream().map(UserDto::new).collect(Collectors.toList());
    }
}
